using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Interface
{
    public interface IPedidoVenda
    {
        IEnumerable<object> ObterTodosPedido();
        IEnumerable<object> ObterVendaPorVendedor(int id);
        int InserirVenda(dto.PedidoVendaDto dto);
        IEnumerable<object> ObterVendaPorId(int id);
        IEnumerable<object> PagamentoAprovado(int id);
        IEnumerable<object> EncaminhadoParaTransportadora(int id);
        IEnumerable<object> DeclararEntrega(int id);
        IEnumerable<object> CancelarPedido(int id);
        IEnumerable<object> DeletarPedido(int id);

    }
}