using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.dto;


namespace tech_test_payment_api.Interface
{
    public interface IVendedor
    {
        IEnumerable<object> InserirNovoVendedor(VendedorDto dto);
        IEnumerable<object> ObteterTodosVendedores();
        IEnumerable<object> ObterVendedorPorId(int id);
        int DeletarVendedor(int id);
        void DeletarTodosVendedores();

    }
}