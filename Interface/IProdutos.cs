using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.dto;

namespace tech_test_payment_api.Interface
{
    public interface IProdutos
    {
        IEnumerable<object>BuscarTodosProdutos();
        IEnumerable<object> InserirProduto(List<ProdutosDto> dto, int idVendedor);
        IEnumerable<object> ObterProdutoPorId(int i);
        IEnumerable<object> ObterPorIdVenda(int i);
        void DeleteTodosProdutos();



    }
}