using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.dto;
using tech_test_payment_api.Interface;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Service
{
    public class VendedorService : IVendedor
    {
        private readonly VendasContext context; 
        public VendedorService(VendasContext context){
            this.context = context;
        }

        public void DeletarTodosVendedores()
        {
            foreach(var item in context.Vendedor.ToList()){
                context.Vendedor.Remove(item);
                context.SaveChanges();
            }
        }

        public int DeletarVendedor(int id)
        {
            var vendedor = context.Vendedor.Find(id);
            if(vendedor == null){
                return 0;
            }
            context.Vendedor.Remove(vendedor);
            context.SaveChanges();
            return id;
        }

        public IEnumerable<object> InserirNovoVendedor(VendedorDto dto)
        {
            if(dto.Nome == string.Empty || dto.EMail == string.Empty || dto.Cpf == string.Empty){

                return null;
            }
            VendedorModel model = new VendedorModel();
            model.Nome = dto.Nome;
            model.Cpf = dto.Cpf;
            model.EMail = dto.EMail;
            var Vendedor = context.Vendedor.Add(model);
            context.SaveChanges();
            return ObterVendedorPorId(model.Id);      
        }

        public IEnumerable<object> ObterVendedorPorId(int id)
        {
            var lista = context.Vendedor.Where(x => x.Id == id);
            return lista;
        }

        public IEnumerable<object> ObteterTodosVendedores()
        {            
            var Vendedores = context.Vendedor.ToList();

            return Vendedores;
        }
    }
}