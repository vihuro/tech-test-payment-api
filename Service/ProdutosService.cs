using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Context;
using tech_test_payment_api.dto;
using tech_test_payment_api.Interface;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Service
{
    public class ProdutosService : IProdutos
    {
        private readonly VendasContext Context;
        private readonly IVendedor Vendedor;
        private readonly IPedidoVenda PedidoVenda;
        private ProdutosModel model;
        public ProdutosService(VendasContext Context, IVendedor Vendedor, IPedidoVenda PedidoVenda){
            this.Context = Context;
            this.Vendedor = Vendedor;
            this.PedidoVenda = PedidoVenda;
        }

        public IEnumerable<object> BuscarTodosProdutos()
        {
            var Produtos = Context.Produtos.ToList();

            return Produtos;
        }

        public void DeleteTodosProdutos()
        {
            foreach(var item in Context.Produtos.ToList()){
                Context.Produtos.Remove(item);
                Context.SaveChanges();
            }
        }

        public IEnumerable<object> InserirProduto(List<ProdutosDto> dto, int idVendedor)
        {
            if(dto.Count() == 0){
                return null;
            }

            if(Vendedor.ObterVendedorPorId(idVendedor).Count() == 0){
                return null;
            }
            PedidoVendaDto venda = new PedidoVendaDto();
            venda.DataHoraVenda = DateTime.Now;
            venda.IdVendedor = idVendedor;
            int idVenda = PedidoVenda.InserirVenda(venda);

            if(venda.Id == null){
                return null;
            }

            foreach(var list in dto){
                model = new ProdutosModel();
                model.Descricao = list.Descricao;
                model.IdVenda = idVenda;
                Context.Produtos.Add(model);
                Context.SaveChanges();
            }

            return ObterPorIdVenda(model.IdVenda);
        }

        public IEnumerable<object> ObterPorIdVenda(int idVenda)
        {
            var ProdutosVendidos = Context.Produtos.Where(x => x.IdVenda == idVenda);
            if(ProdutosVendidos.Count() == 0){
                return null;
            }

            return ProdutosVendidos;
        }

        public IEnumerable<object> ObterProdutoPorId(int id)
        {
            var Produto = Context.Produtos.Where(x => x.Id == id);
            if(Produto.Count() == 0){
                return null;
            }

            return Produto;
        }
    }
}