using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Context;
using tech_test_payment_api.dto;
using tech_test_payment_api.Interface;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Service
{
    public class PedidoVendaService : IPedidoVenda
    {
        private readonly VendasContext context;
        public PedidoVendaService(VendasContext context){
            this.context = context;
        }

        private PedidoVenda model;

        public IEnumerable<object> CancelarPedido(int id)
        {
            var PedidoVenda = context.Pedidos.Find(id);
            
            if(PedidoVenda.StatusVenda != "Aguardando Pagamento" || 
                PedidoVenda.StatusVenda == "Encaminhado Para a Transportadora" ||
                PedidoVenda.StatusVenda == "Entegue"){
                
                return null; 
            }

            PedidoVenda.StatusVenda = "Cancelado";
            context.Update(PedidoVenda);
            context.SaveChanges();
            
            return ObterVendaPorId(PedidoVenda.Id);
        }

        public IEnumerable<object> DeclararEntrega(int id)
        {
            var PedidoVenda = context.Pedidos.Find(id);
            
            if(PedidoVenda.StatusVenda != "Encaminhado Para a Transportadora"){
                
                return null; 
            }

            PedidoVenda.StatusVenda = "Entregue";
            context.Update(PedidoVenda);
            context.SaveChanges();
            
            return ObterVendaPorId(PedidoVenda.Id);
        }

        public IEnumerable<object> DeletarPedido(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> EncaminhadoParaTransportadora(int id)
        {
            var PedidoVenda = context.Pedidos.Find(id);
            if(PedidoVenda.StatusVenda != "Pagamento Aprovado"){
                
                return null; 
            }

            PedidoVenda.StatusVenda = "Encaminhado Para a Transportadora";
            context.Update(PedidoVenda);
            context.SaveChanges();
            
            return ObterVendaPorId(PedidoVenda.Id);
        }

        public int InserirVenda(PedidoVendaDto dto)
        {
            model = new PedidoVenda();
            model.DataHoraVenda = DateTime.Now;
            model.IdVendedor = dto.IdVendedor;
            model.StatusVenda = "Aguardando Pagamento";

            context.Pedidos.Add(model);
            context.SaveChanges();

            return model.Id;
        }

        public IEnumerable<object> ObterTodosPedido()
        {
            var PedidoVenda = context.Pedidos.ToList();

            return PedidoVenda;
        }

        public IEnumerable<object> ObterVendaPorId(int id)
        {
            var Pedidos = context.Pedidos.Where(x => x.Id == id);
            
            return Pedidos;

        }

        public IEnumerable<object> ObterVendaPorVendedor(int idVendedor)
        {
            var Pedidos = context.Pedidos.Where(x => x.IdVendedor == idVendedor);
            return Pedidos;
        }

        public IEnumerable<object> PagamentoAprovado(int id)
        {
            var PedidoVenda = context.Pedidos.Find(id);
            if(PedidoVenda.StatusVenda != "Aguardando Pagamento"){
                
                return null; 
            }

            PedidoVenda.StatusVenda = "Pagamento Aprovado";
            context.Update(PedidoVenda);
            context.SaveChanges();
            
            return ObterVendaPorId(PedidoVenda.Id);
        }
    }
}