using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.dto;
using tech_test_payment_api.Interface;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutosController : ControllerBase
    {
        private readonly IProdutos service;
        public ProdutosController(IProdutos service){
            this.service = service;

        }
        [HttpGet]
        public IActionResult BuscarProdutos(){
            
            var Produtos = service.BuscarTodosProdutos();
            if(Produtos == null){
                NotFound("Pedido não encontrados");
            }

            return Ok(Produtos);
        }
        [HttpPost]
        public IActionResult IncluirPedido(List<ProdutosDto> dto , int IdVendedor){

            var Venda = service.InserirProduto(dto, IdVendedor);
            if(Venda == null){
                return Unauthorized("Lista sem itens ou vendedor invalido");
            }
            return Ok(Venda);
        }

        [HttpGet("ObterPorID")]
        public IActionResult obterProduto(int id)
        {
            var Produtos = service.ObterProdutoPorId(id);
            if(Produtos.Count() == 0){
                return NotFound("Venda não encontrada!");

            }
            return Ok(Produtos);
        }

        [HttpGet("OberterPorIdVenda")]
        public IActionResult ObterPorIdVenda(int idVenda){
            var Produto = service.ObterPorIdVenda(idVenda);
            if(Produto.Count() == 0){
                NotFound("Venda não encontrada");
            }

            return Ok(Produto);
        }

        
        [HttpDelete("DeleteTudo")]
        public IActionResult DeletarTudo(){
            
            service.DeleteTodosProdutos();
            return Ok();

        }
    }
}