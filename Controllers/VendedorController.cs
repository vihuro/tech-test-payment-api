using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Service;
using tech_test_payment_api.dto;
using tech_test_payment_api.Interface;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly IVendedor service;
        public VendedorController(IVendedor service){
            this.service = service;
        }


       [HttpGet("RetornarTodosVendedores")]  
       public IActionResult  RetornarLista(){

            var lista = service.ObteterTodosVendedores();
            if(lista == null){
                return NotFound();
            }

            return Ok(lista);
       }


        [HttpGet("RetornarVendedorPorID")]
        public IActionResult ObterPorId(int id){
            var Vendedor = service.ObterVendedorPorId(id);
            if(Vendedor.Count() == 0){
                return NotFound();
            }

            return Ok(Vendedor);
        }


        [HttpPost]
        public IActionResult CadastrarVendedor(VendedorDto dto){
            
            var Vendedor = service.InserirNovoVendedor(dto);
            if(Vendedor == null){
                return Unauthorized();
            }

            return Ok(Vendedor);
        }

        [HttpDelete("{id}")]
        public IActionResult DeletarVendedor(int id){

            var Deletar = service.DeletarVendedor(id);
            if(Deletar == 0){
                return NotFound("Vendedor não econtrado");
            }
            return Ok($"Vendedor de ID {id} deletado");
        }
        [HttpDelete("DeleteTodosVendedores")]
        public IActionResult DeletarTodosVendedores(){
            try{
            service.DeletarTodosVendedores();
            return Ok("Todos Vendedores deletados");
            }
            catch{
                return BadRequest("Erro interno");

            }
        }
        
    }
}