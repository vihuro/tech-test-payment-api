using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Interface;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PedidosVendaController : ControllerBase
    {
        public VendasContext context;
        private VendedorController vendedor;
        private readonly IPedidoVenda service;
        public PedidosVendaController(IPedidoVenda service){
            this.service = service;
        }


        [HttpGet("ObterTodosPedidos")]
        public IActionResult ObterTodosPedidos()
        {

            return Ok(service.ObterTodosPedido());
        }

        [HttpGet("BuscarPorVendedor")]
        public IActionResult VendasPorVendedor(int id){
            
            var Vendas = service.ObterVendaPorVendedor(id);
            
            if(Vendas.Count() == 0){
                return NotFound("Vendas não registradas para esse vendedor");
            }
            return Ok(Vendas);
        }


        [HttpGet("BuscarPorID")]
        public IActionResult ObterVenda(int id)
        {
            var Vendas = service.ObterVendaPorId(id);
            if(Vendas.Count() == 0){
                return NotFound("Não foi possivel encontrar essa venda!");
            }

            return Ok(Vendas);
        }

        [HttpPut("PagamentoAutorizado{id}")]
        public IActionResult PagamentoAprovado(int id){
            var pedido = service.PagamentoAprovado(id);
            if(pedido.Count() == 0){
                return NotFound("Pedido não encotrado!");
            }
            else if(pedido == null){
                return Unauthorized("Não é possivel alterar este pedido");
            }

            return Ok(pedido);
            
        }

        [HttpPut("EncaminhadoTransportadora{id}")]
        public IActionResult EncaminhadoTransportadora(int id){
            var pedido = service.EncaminhadoParaTransportadora(id);
            if(pedido.Count() == 0){
                return NotFound("Pedido não encotrado!");
            }
            else if(pedido == null){
                return Unauthorized("Não é possivel alterar este pedido");
            }

            return Ok(pedido);
            
        }

        [HttpPut("DeclaraEntrega{id}")]
        public IActionResult DeclararEntrega(int id){
            var pedido = service.DeclararEntrega(id);
            if(pedido.Count() == 0){
                return NotFound("Pedido não encotrado!");
            }
            else if(pedido == null){
                return Unauthorized("Não é possivel alterar este pedido");
            }

            return Ok(pedido);        
        }

        [HttpPut("CancelarPedido{id}")]
        public IActionResult Cancelar(int id){
            var pedido = service.CancelarPedido(id);
            if(pedido.ToList().Count() == 0 ){
                return NotFound("Pedido não encotrado!");
            }
            else if(pedido == null){
                return Unauthorized("Não é possivel alterar este pedido");
            }

            return Ok(pedido);      
            
        }


        [HttpDelete("{id}")]
        public IActionResult DeletarPedido(int id){
            var pedido = context.Pedidos.Find(id);
            if(pedido == null){
                return NotFound("Pedido não econtrado!");
            }
            context.Remove(id);
            context.SaveChanges();
            return Ok("Pedido deletado com sucesso!");
        }
        [HttpDelete("DeleteTudo")]
        public IActionResult DeleteTudo(){
            var delete = context.Pedidos.ToList();
            foreach(var id in context.Pedidos.ToList()){
                context.Remove(id);
            }
  
            context.SaveChanges();
            return Ok();
        }
    }
}