using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.dto
{
    public class VendedorDto
    {
        private int id;
        private string nome;
        private string eMail;
        private string cpf;

        public int Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        public string EMail { get => eMail; set => eMail = value; }
        public string Cpf { get => cpf; set => cpf = value; }
        
    }
}