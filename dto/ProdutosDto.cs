using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.dto
{
    public class ProdutosDto
    {
        private int id;
        private string descricao;
        private int idVenda;

        public int Id { get => id; set => id = value; }
        public string Descricao { get => descricao; set => descricao = value; }
        public int IdVenda { get => idVenda; set => idVenda = value; }
        
    }
}